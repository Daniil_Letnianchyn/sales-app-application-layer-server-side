﻿using SalesApp.ApplicationLayer.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.ApplicationLayer.Infrastructure.Inventory
{
    public class InventoryService : IInventoryService
    {
        public void NotifySaleOcurred()
        {
            NotifyNoOne();
        }

        private void NotifyNoOne()
        {
        }
    }
}
