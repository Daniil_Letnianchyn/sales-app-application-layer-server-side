﻿using SalesApp.ApplicationLayer.Domain.Customers;
using SalesApp.ApplicationLayer.Domain.Employees;
using SalesApp.ApplicationLayer.Domain.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.ApplicationLayer.Domain.Sales
{
    public class Sale
    {
        private int _quantity;
        private decimal _totalPrice;
        private decimal _unitPrice;
        private bool _isSaleAlreadyAplied;
        private Guid _saleId;


        public Sale()
        {
            _isSaleAlreadyAplied = false;
        }

        public string SaleId
        {
            get { return _saleId.ToString(); }
            set
            {
                _saleId = Guid.Parse(value);
            }
        }

        public DateTimeOffset Date { get; set; }

        public Customer Customer { get; set; }

        public Employee Employee { get; set; }

        public Product Product { get; set; }

        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set
            {
                _unitPrice = value;
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                UpdateUnitPrice();
            }
        }

        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set
            {
                _totalPrice = value;
                UpdateUnitPrice();
            }
        }

        public void ApplyRandomSale()
        {
            if (!_isSaleAlreadyAplied)
            {
                UnitPrice = UnitPrice / 100 * new Random().Next(50, 99);
                _isSaleAlreadyAplied = true;
            }
        }

        private void UpdateTotalPrice()
        {
            _totalPrice = _unitPrice * _quantity;
        }

        private void UpdateUnitPrice()
        {
            if (_totalPrice != 0 && _quantity != 0)
                _unitPrice = _totalPrice / _quantity;
        }
    }
}
