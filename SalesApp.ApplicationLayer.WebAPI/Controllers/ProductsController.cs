﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.ApplicationLayer.Application.Services.Products;
using SalesApp.ApplicationLayer.Application.Services.Sales.Exceptions;

namespace SalesApp.ApplicationLayer.WebAPI.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IProductsService _productsService;

        public ProductsController(IProductsService productsService)
        {
            this._productsService = productsService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetProductsAsync()
        {
            try
            {
                return Ok(await _productsService.GetAllProductsAsync());
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}