﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.ApplicationLayer.Application.Services.Customers;
using SalesApp.ApplicationLayer.Application.Services.Sales.Exceptions;

namespace SalesApp.ApplicationLayer.WebAPI.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private ICustomersService _customersService;

        public CustomersController(ICustomersService customersService)
        {
            this._customersService = customersService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetCustomersAsync()
        {
            try
            {
                return Ok(await _customersService.GetAllCustomersAsync());
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}