﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.ApplicationLayer.Application.Services.Sales;
using SalesApp.ApplicationLayer.Application.Services.Sales.Exceptions;

namespace SalesApp.ApplicationLayer.WebAPI.Controllers
{
    [Route("api/sales")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private ISalesService _salesService;

        public SalesController(ISalesService salesService)
        {
            this._salesService = salesService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetSalesAsync()
        {
            try
            {
                return Ok(await _salesService.GetAllSalesAsync());
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("{saleId}")]
        public async Task<IActionResult> GetSaleByIdAsync(string saleId)
        {
            try
            {
                return Ok(await _salesService.GetSaleBySaleIdAsync(saleId));
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost()]
        public async Task<IActionResult> AddSaleAsync([FromBody] Application.DataTransferObjects.Incoming.SaleForCreation saleForCreationDTO)
        {
            try
            {
                return Ok(await _salesService.CreateNewSaleAsync(saleForCreationDTO));
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}