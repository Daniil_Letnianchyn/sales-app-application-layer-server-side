﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.ApplicationLayer.Application.Services.Employees;
using SalesApp.ApplicationLayer.Application.Services.Sales.Exceptions;

namespace SalesApp.ApplicationLayer.WebAPI.Controllers
{
    [Route("api/employees")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private IEmployeesService _employeesService;

        public EmployeesController(IEmployeesService employeesService)
        {
            this._employeesService = employeesService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetEmployeesAsync()
        {
            try
            {
                return Ok(await _employeesService.GetAllEmployeesAsync());
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}