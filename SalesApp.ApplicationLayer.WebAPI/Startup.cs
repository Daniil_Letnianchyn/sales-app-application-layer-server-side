using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SalesApp.ApplicationLayer.Application.Interfaces;
using SalesApp.ApplicationLayer.Application.Services.Customers;
using SalesApp.ApplicationLayer.Application.Services.Employees;
using SalesApp.ApplicationLayer.Application.Services.Products;
using SalesApp.ApplicationLayer.Application.Services.Sales;
using SalesApp.ApplicationLayer.Common.Logger;
using SalesApp.ApplicationLayer.Infrastructure.Inventory;
using SalesApp.ApplicationLayer.Persistence;

namespace SalesApp.ApplicationLayer.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<DatabaseContext>(options =>
            {
                options.UseSqlServer(
                    @"Server=(localdb)\mssqllocaldb;Database=SalesAppDB;Trusted_Connection=True;");
            });

            services.AddScoped<IDatabaseService, DatabaseService>();
            services.AddScoped<IInventoryService, InventoryService>();
            services.AddScoped<ISalesService, SalesService>();
            services.AddScoped<IProductsService, ProductsService>();
            services.AddScoped<IEmployeesService, EmployeesService>();
            services.AddScoped<ICustomersService, CustomersService>();
            services.AddSingleton<ICustomLogger, CustomLogger>();

            // Configure CORS so the API allows requests from JavaScript.  
            // For demo purposes, all origins/headers/methods are allowed.  
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOriginsHeadersAndMethods",
                    builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors("AllowAllOriginsHeadersAndMethods");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
