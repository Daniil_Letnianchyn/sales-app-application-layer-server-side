﻿using SalesApp.ApplicationLayer.Application.DataTransferObjects.Incoming;
using SalesApp.ApplicationLayer.Application.DataTransferObjects.Outgoing;
using SalesApp.ApplicationLayer.Application.Interfaces;
using SalesApp.ApplicationLayer.Application.Services.Sales.Exceptions;
using SalesApp.ApplicationLayer.Common.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.ApplicationLayer.Application.Services.Sales
{
    public class SalesService : ISalesService
    {
        private IDatabaseService _databaseService;
        private ICustomLogger _logger;
        private IInventoryService _inventory;

        public SalesService(IDatabaseService databaseService, ICustomLogger logger, IInventoryService inventory)
        {
            this._databaseService = databaseService;
            this._logger = logger;
            this._inventory = inventory;
        }

        public async Task<Sale> CreateNewSaleAsync(SaleForCreation saleForCreation)
        {

            try
            {
                Guid.Parse(saleForCreation.CustomerId);
            }
            catch(FormatException ex)
            {
                throw new InvalidGuidException($@"{saleForCreation.CustomerId}",$@"Imaginary-correlation-id");
            }

            Domain.Sales.Sale newSale = new Domain.Sales.Sale
            {
                Customer = new Domain.Customers.Customer { CustomerId = saleForCreation.CustomerId },
                Employee = new Domain.Employees.Employee { EmployeeId = saleForCreation.EmployeeId },
                Product = new Domain.Products.Product { ProductId = saleForCreation.ProductId },
                Quantity = saleForCreation.Quantity
            };

            Domain.Sales.Sale sale = await _databaseService.CreateSaleAsync(newSale);
            _logger.Info($@"Sale {sale.SaleId} was crated");
            _inventory.NotifySaleOcurred();

            Sale saleToReturn = new DataTransferObjects.Outgoing.Sale
            {
                Id = sale.SaleId,
                Customer = sale.Customer.Name,
                Employee = sale.Employee.Name,
                Product = sale.Product.Name,
                Date = sale.Date,
                Quantity = sale.Quantity,
                TotalPrice = sale.TotalPrice,
                UnitPrice = sale.UnitPrice
            };
            return saleToReturn;
        }

        public async Task<Sale> GetSaleBySaleIdAsync(string saleId)
        {
            Domain.Sales.Sale sale = await _databaseService.GetSaleByIdAsync(saleId);

            if (sale == null)
            {
                throw new NullReferenceException();
            }

            DataTransferObjects.Outgoing.Sale saleToReturnDTO = new DataTransferObjects.Outgoing.Sale
            {
                Id = sale.SaleId,
                Customer = sale.Customer.Name,
                Employee = sale.Employee.Name,
                Product = sale.Product.Name,
                Date = sale.Date,
                Quantity = sale.Quantity,
                TotalPrice = sale.TotalPrice,
                UnitPrice = sale.UnitPrice
            };

            return saleToReturnDTO;
        }

        public async Task<IEnumerable<Sale>> GetAllSalesAsync()
        {
            IEnumerable<SalesApp.ApplicationLayer.Domain.Sales.Sale> allSales = await _databaseService.GetAllSalesAsync();

            IEnumerable<DataTransferObjects.Outgoing.Sale> allSalesDTO = allSales
                .Select(b => new DataTransferObjects.Outgoing.Sale
                {
                    Id = b.SaleId,
                    Customer = b.Customer.Name,
                    Employee = b.Employee.Name,
                    Product = b.Product.Name,
                    Date = b.Date,
                    Quantity = b.Quantity,
                    TotalPrice = b.TotalPrice,
                    UnitPrice = b.UnitPrice
                });
            return allSalesDTO;
        }
    }
}
