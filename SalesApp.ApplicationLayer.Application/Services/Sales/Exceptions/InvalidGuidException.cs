﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.ApplicationLayer.Application.Services.Sales.Exceptions
{
    [Serializable]
    public class InvalidGuidException : ApplicationException
    {
        public string CorrelationId {get;}

        public InvalidGuidException(string correlationId)
        {
            this.CorrelationId = correlationId;
        }

        public InvalidGuidException(string invalidGuid, string correlationId)
            : base($@"Invalid Guid: {invalidGuid}")
        {
            this.CorrelationId = correlationId;
        }

    }
}
