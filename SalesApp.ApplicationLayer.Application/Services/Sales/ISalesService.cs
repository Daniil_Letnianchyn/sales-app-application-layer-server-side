﻿using SalesApp.ApplicationLayer.Application.DataTransferObjects.Incoming;
using SalesApp.ApplicationLayer.Application.DataTransferObjects.Outgoing;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.ApplicationLayer.Application.Services.Sales
{
    public interface ISalesService
    {
        public Task<Sale> CreateNewSaleAsync(SaleForCreation saleForCreation);
        public Task<IEnumerable<Sale>> GetAllSalesAsync();
        public Task<Sale> GetSaleBySaleIdAsync(string saleId);
    }
}
