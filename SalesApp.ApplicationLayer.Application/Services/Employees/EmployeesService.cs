﻿using SalesApp.ApplicationLayer.Application.DataTransferObjects.Outgoing;
using SalesApp.ApplicationLayer.Application.Interfaces;
using SalesApp.ApplicationLayer.Common.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.ApplicationLayer.Application.Services.Employees
{
    public class EmployeesService : IEmployeesService
    {
        private IDatabaseService _databaseService;
        private ICustomLogger _logger;
        private IInventoryService _inventory;

        public EmployeesService(IDatabaseService databaseService, ICustomLogger logger, IInventoryService inventory)
        {
            this._databaseService = databaseService;
            this._logger = logger;
            this._inventory = inventory;
        }

        public async Task<IEnumerable<Employee>> GetAllEmployeesAsync()
        {
            IEnumerable<Domain.Employees.Employee> allEmployees = await _databaseService.GetAllEmployeesAsync();

            IEnumerable<Employee> allEmployeesDTO = allEmployees
                .Select(b => new Employee
                {
                    Id = b.EmployeeId,
                    Name = b.Name
                });
            return allEmployeesDTO;
        }
    }
}
