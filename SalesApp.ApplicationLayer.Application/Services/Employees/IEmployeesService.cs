﻿using SalesApp.ApplicationLayer.Application.DataTransferObjects.Outgoing;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.ApplicationLayer.Application.Services.Employees
{
    public interface IEmployeesService
    {
        public Task<IEnumerable<Employee>> GetAllEmployeesAsync();
    }
}
