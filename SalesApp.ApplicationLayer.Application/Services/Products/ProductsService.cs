﻿using SalesApp.ApplicationLayer.Application.DataTransferObjects.Outgoing;
using SalesApp.ApplicationLayer.Application.Interfaces;
using SalesApp.ApplicationLayer.Common.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.ApplicationLayer.Application.Services.Products
{
    public class ProductsService : IProductsService
    {
        private IDatabaseService _databaseService;
        private ICustomLogger _logger;
        private IInventoryService _inventory;

        public ProductsService(IDatabaseService databaseService, ICustomLogger logger, IInventoryService inventory)
        {
            this._databaseService = databaseService;
            this._logger = logger;
            this._inventory = inventory;
        }

        public async Task<IEnumerable<Product>> GetAllProductsAsync()
        {
            IEnumerable<Domain.Products.Product> allProducts = await _databaseService.GetAllProductsAsync();

            IEnumerable<DataTransferObjects.Outgoing.Product> allPproductsDTO = allProducts
                .Select(b => new DataTransferObjects.Outgoing.Product
                {
                    Id = b.ProductId,
                    Name = b.Name,
                    Price = b.Price
                });

            return allPproductsDTO;
        }
    }
}
