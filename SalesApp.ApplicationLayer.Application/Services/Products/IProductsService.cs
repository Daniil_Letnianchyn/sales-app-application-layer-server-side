﻿using SalesApp.ApplicationLayer.Application.DataTransferObjects.Outgoing;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.ApplicationLayer.Application.Services.Products
{
    public interface IProductsService
    {
        public Task<IEnumerable<Product>> GetAllProductsAsync();
    }
}
