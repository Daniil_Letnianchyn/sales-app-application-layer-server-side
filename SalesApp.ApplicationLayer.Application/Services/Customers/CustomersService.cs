﻿using SalesApp.ApplicationLayer.Application.DataTransferObjects.Outgoing;
using SalesApp.ApplicationLayer.Application.Interfaces;
using SalesApp.ApplicationLayer.Common.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.ApplicationLayer.Application.Services.Customers
{
    public class CustomersService : ICustomersService
    {
        private IDatabaseService _databaseService;
        private ICustomLogger _logger;
        private IInventoryService _inventory;

        public CustomersService(IDatabaseService databaseService, ICustomLogger logger, IInventoryService inventory)
        {
            this._databaseService = databaseService;
            this._logger = logger;
            this._inventory = inventory;
        }

        public async Task<IEnumerable<Customer>> GetAllCustomersAsync()
        {
            IEnumerable<Domain.Customers.Customer> allCustomers = await _databaseService.GetAllCustomersAsync();

            IEnumerable<DataTransferObjects.Outgoing.Customer> allCustomersDTO = allCustomers
                .Select(b => new DataTransferObjects.Outgoing.Customer
                {
                    Id = b.CustomerId,
                    Name = b.Name
                });
            return allCustomersDTO;
        }
    }
}
