﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.ApplicationLayer.Application.Interfaces
{
    public interface IDatabaseService
    {
        Task<IEnumerable<SalesApp.ApplicationLayer.Domain.Sales.Sale>> GetAllSalesAsync();
        Task<SalesApp.ApplicationLayer.Domain.Sales.Sale> GetSaleByIdAsync(string saleId);
        Task<SalesApp.ApplicationLayer.Domain.Sales.Sale> CreateSaleAsync(SalesApp.ApplicationLayer.Domain.Sales.Sale saleForCreation);
        Task<IEnumerable<SalesApp.ApplicationLayer.Domain.Products.Product>> GetAllProductsAsync();
        Task<IEnumerable<SalesApp.ApplicationLayer.Domain.Employees.Employee>> GetAllEmployeesAsync();
        Task<IEnumerable<SalesApp.ApplicationLayer.Domain.Customers.Customer>> GetAllCustomersAsync();
    }
}
