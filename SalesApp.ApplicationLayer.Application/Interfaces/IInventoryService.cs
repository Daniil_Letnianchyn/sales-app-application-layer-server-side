﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.ApplicationLayer.Application.Interfaces
{
    public interface IInventoryService
    {
        public void NotifySaleOcurred();
    }
}
