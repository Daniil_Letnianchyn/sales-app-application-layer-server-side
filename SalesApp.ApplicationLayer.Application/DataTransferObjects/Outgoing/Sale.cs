﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.ApplicationLayer.Application.DataTransferObjects.Outgoing
{
    public class Sale
    {
        public string Id { get; set; }
        public DateTimeOffset Date { get; set; }
        public string Customer { get; set; }
        public string Employee { get; set; }
        public string Product { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
