﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.ApplicationLayer.Persistence.Entities.Interfaces
{
    public interface IModificationHistory
    {
        DateTimeOffset DateModified { get; set; }
        DateTimeOffset DateCreated { get; set; }
    }
}
